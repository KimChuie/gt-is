

package capstone;

import static capstone.EmpInformation.rs;
import static capstone.EmpInformation.smt;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;

public class method extends dbConnect{
    public static Connection con = dbConnect.connect();
    public static Statement smt = null;
    public static ResultSet rs = null;
    public static PreparedStatement ps = null;
    
    public static void loginCommand(String sql) {
        try {
            smt = con.createStatement();
            rs = smt.executeQuery(sql);

            if (rs.next()) {

                JOptionPane.showMessageDialog(null, "WELCOME!");

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public static void insertionCommand(String sql, String prompt) {
        try {
            smt = con.createStatement();
            smt.execute(sql);
            JOptionPane.showMessageDialog(null, prompt);
            smt.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);

        }

    }

    public static void CheckUser(String sql, JFrame newFrame, JFrame oldFrame) {

        try {
            smt = con.createStatement();
            rs = smt.executeQuery(sql);
            if (rs.next()) {

                newFrame.setVisible(true);
                oldFrame.dispose();
                JOptionPane.showMessageDialog(null, "Success!");

            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }
    public static void combo (JComboBox combo, String query, String column ){
        try {
            combo.removeAllItems();
             smt = con.createStatement();
            rs = smt.executeQuery(query);
            while(rs.next()){
            combo.addItem(rs.getString(column));
            }
            rs.close();
            smt.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            
        }
    }

    public static void displayRows(JTable table, String sql) {

        try {
            smt = con.createStatement();
            rs = smt.executeQuery(sql);
            table.setModel(DbUtils.resultSetToTableModel(rs));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    

    public static void Exe(String sql, String prompt) {
        try {
            smt = con.createStatement();
            smt.execute(sql);
            JOptionPane.showMessageDialog(null, "Succesfully!");
            smt.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e, "error!", JOptionPane.ERROR_MESSAGE);
        }
    }
    void office(JComboBox combo, String query, String column) {
        try {
            combo.removeAll();
            smt = con.createStatement();
            rs = smt.executeQuery(query);
            while (rs.next()) {
                combo.addItem(rs.getString(column));
            }
            rs.close();
            smt.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }
  
}
