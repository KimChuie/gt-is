/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package capstone;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chuie
 */
public class functions {
    public static ResultSet exeq(String query){
        try {
            Connection con = dbConnect.connect();
            Statement stt = con.createStatement();
            ResultSet res = stt.executeQuery(query);
            return res;
        } catch (SQLException ex) {
            Logger.getLogger(functions.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static void exe(String query){
        try {
            Connection con = dbConnect.connect();
            Statement stt = con.createStatement();
            stt.execute(query);
        } catch (SQLException ex) {
            Logger.getLogger(functions.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
