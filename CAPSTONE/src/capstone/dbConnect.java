

package capstone;

import java.sql.*;
import javax.swing.*;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dbConnect {
    public static Connection connect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String driver = "jdbc:mysql:";
            String address = "//localhost:3306/";
            String dbname = "capstone";
            String url = driver + address + dbname;
            String username = "root", password = "";
            Connection con = DriverManager.getConnection(url, username, password);
            return con;

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }

    public static void main(String[] args) {
        Connection v = connect();
        if (v != null) {
            JOptionPane.showMessageDialog(null, "success");
        } else {
            JOptionPane.showMessageDialog(null, "error");
        }

    }
    
}
