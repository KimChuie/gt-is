-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 10:32 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `capstone`
--

-- --------------------------------------------------------

--
-- Table structure for table `civil_status`
--

CREATE TABLE `civil_status` (
  `status_id` int(11) NOT NULL,
  `civil_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `civil_status`
--

INSERT INTO `civil_status` (`status_id`, `civil_status`) VALUES
(2, 'Married'),
(3, 'Separated'),
(1, 'Single'),
(4, 'Widowed');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `Attitude` text NOT NULL,
  `Work_under_pressure` text NOT NULL,
  `Leadership` text NOT NULL,
  `Attendance/Punctuality` text NOT NULL,
  `Responsibility` text NOT NULL,
  `Willingness` text NOT NULL,
  `Creativity` text NOT NULL,
  `Productivity` text NOT NULL,
  `Initiative` text NOT NULL,
  `Employee_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `Attitude`, `Work_under_pressure`, `Leadership`, `Attendance/Punctuality`, `Responsibility`, `Willingness`, `Creativity`, `Productivity`, `Initiative`, `Employee_ID`) VALUES
(1, 'safsdf', 'safsaf', '', '', '', '', '', '', '', 72),
(2, '', '', 'Great', 'jkhljhjkhlkhjljh', '12314124', '', '', '', '', 67),
(3, '', '', 'Great', 'jkhljhjkhlkhjljh', '12314124', '', '', '', '', 67);

-- --------------------------------------------------------

--
-- Table structure for table `employment_status`
--

CREATE TABLE `employment_status` (
  `status_id` int(11) NOT NULL,
  `employment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment_status`
--

INSERT INTO `employment_status` (`status_id`, `employment_status`) VALUES
(2, 'Provisionary'),
(1, 'Regular'),
(3, 'Trainee');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `userID` int(2) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`userID`, `username`, `password`) VALUES
(1, 'gtts', 'gtts'),
(2, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mechanics`
--

CREATE TABLE `mechanics` (
  `mechanics_id` int(11) NOT NULL,
  `mechanics` varchar(255) NOT NULL,
  `descript` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `score_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `equi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scores`
--

INSERT INTO `scores` (`score_id`, `score`, `equi`) VALUES
(1, 1, 'Poor'),
(2, 2, 'Fair'),
(3, 3, 'Average'),
(4, 4, 'Good'),
(5, 5, 'Excellent');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assignment`
--

CREATE TABLE `tbl_assignment` (
  `assignment_ID` int(2) NOT NULL,
  `assignment_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_assignment`
--

INSERT INTO `tbl_assignment` (`assignment_ID`, `assignment_name`) VALUES
(1, 'Assignment1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_designation`
--

CREATE TABLE `tbl_designation` (
  `designation_ID` int(2) NOT NULL,
  `designation_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_designation`
--

INSERT INTO `tbl_designation` (`designation_ID`, `designation_name`) VALUES
(1, 'Office'),
(2, 'Motorpool'),
(3, 'Maintenance'),
(4, 'Lime Spreader Helper'),
(5, 'Driver');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_driver`
--

CREATE TABLE `tbl_driver` (
  `driver_ID` int(2) NOT NULL,
  `driver_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_driver`
--

INSERT INTO `tbl_driver` (`driver_ID`, `driver_name`) VALUES
(1, 'RDT'),
(2, 'PDT'),
(3, 'Forward'),
(4, 'Tanker'),
(5, 'SDT'),
(6, 'Ten Wheelers');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `Employee_ID` int(2) NOT NULL,
  `FirstName` text NOT NULL,
  `MiddleName` text NOT NULL,
  `LastName` text NOT NULL,
  `Gender` text NOT NULL,
  `Birthday` date NOT NULL,
  `Address` varchar(50) NOT NULL,
  `ContactNumber` varchar(11) NOT NULL,
  `Employment_Date` date NOT NULL,
  `designation_ID` int(11) NOT NULL,
  `assignment_ID` int(11) NOT NULL,
  `Employment_Status` int(11) NOT NULL,
  `sss_no` varchar(25) NOT NULL,
  `philhealth` varchar(25) NOT NULL,
  `tin_no` varchar(25) NOT NULL,
  `fathers_name` text NOT NULL,
  `mothers_name` text NOT NULL,
  `CivilStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`Employee_ID`, `FirstName`, `MiddleName`, `LastName`, `Gender`, `Birthday`, `Address`, `ContactNumber`, `Employment_Date`, `designation_ID`, `assignment_ID`, `Employment_Status`, `sss_no`, `philhealth`, `tin_no`, `fathers_name`, `mothers_name`, `CivilStatus`) VALUES
(67, 'Abad', 'A', 'Heraclio', 'Male', '2018-05-27', 'Manolo', '34324324', '1997-05-13', 1, 1, 1, '3435', '346547', '131', 'Fernando', 'Helena', 1),
(72, 'Kouim', '', 'Joshua', 'Male', '1999-01-03', 'Manolo', '09360450393', '2018-10-17', 3, 1, 3, '16161616', '45454545', '69696969', 'Father', 'Mother', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluatedby`
--

CREATE TABLE `tbl_evaluatedby` (
  `evaluatedID` int(2) NOT NULL,
  `evaluatedName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluatedby`
--

INSERT INTO `tbl_evaluatedby` (`evaluatedID`, `evaluatedName`) VALUES
(1, 'Calacat, Annabelle'),
(2, 'Clarabal, Maria May'),
(3, 'Ferrer, Angelene'),
(4, 'Nieves, Ann');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation`
--

CREATE TABLE `tbl_evaluation` (
  `evaluation_ID` int(2) NOT NULL,
  `Employee_ID` int(2) NOT NULL,
  `evaluated_by` int(11) NOT NULL,
  `date` date NOT NULL,
  `Attitude` float NOT NULL,
  `Work_under_pressure` float NOT NULL,
  `Leadership` float NOT NULL,
  `Attendance/Punctuality` float NOT NULL,
  `Responsibility` float NOT NULL,
  `Willingness` float NOT NULL,
  `Creativity` float NOT NULL,
  `Productivity` float NOT NULL,
  `Initiative` float NOT NULL,
  `Total` double NOT NULL,
  `remarks` text NOT NULL,
  `comments` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation`
--

INSERT INTO `tbl_evaluation` (`evaluation_ID`, `Employee_ID`, `evaluated_by`, `date`, `Attitude`, `Work_under_pressure`, `Leadership`, `Attendance/Punctuality`, `Responsibility`, `Willingness`, `Creativity`, `Productivity`, `Initiative`, `Total`, `remarks`, `comments`) VALUES
(4, 72, 2, '2018-10-18', 5, 2, 3, 1, 1, 1, 1, 1, 1, 1, 'Poor', 1),
(5, 67, 4, '2018-10-18', 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 'Excellent', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_items`
--

CREATE TABLE `tbl_items` (
  `itemID` int(2) NOT NULL,
  `ItemName` text NOT NULL,
  `Mechanics` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lime_spreader_helper`
--

CREATE TABLE `tbl_lime_spreader_helper` (
  `lime_spreader_helper_ID` int(2) NOT NULL,
  `lime_spreader_helper_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_lime_spreader_helper`
--

INSERT INTO `tbl_lime_spreader_helper` (`lime_spreader_helper_ID`, `lime_spreader_helper_name`) VALUES
(1, 'Lime spreader helper');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_maintenance`
--

CREATE TABLE `tbl_maintenance` (
  `maintenance_ID` int(2) NOT NULL,
  `maintenance name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_maintenance`
--

INSERT INTO `tbl_maintenance` (`maintenance_ID`, `maintenance name`) VALUES
(1, 'Welder Trainee'),
(2, 'Underchasis Trainee'),
(3, 'Electrician Trainee'),
(4, 'Machinist Trainee'),
(5, 'Utility');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_motorpool`
--

CREATE TABLE `tbl_motorpool` (
  `motorpool_ID` int(2) NOT NULL,
  `Motorpoll Name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_motorpool`
--

INSERT INTO `tbl_motorpool` (`motorpool_ID`, `Motorpoll Name`) VALUES
(1, 'Electrician'),
(2, 'Welder Trainee'),
(3, 'Underchasis Trainee'),
(4, 'Underchasis'),
(5, 'Mechanic'),
(6, 'Welder'),
(7, 'Underchasis/Gasman'),
(8, 'Tireman'),
(9, 'Machinist'),
(10, 'Carpenter');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_office`
--

CREATE TABLE `tbl_office` (
  `Office_ID` int(2) NOT NULL,
  `office_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_office`
--

INSERT INTO `tbl_office` (`Office_ID`, `office_name`) VALUES
(1, 'Liaison/Coordinator'),
(2, 'Stockman'),
(3, 'Office Staff'),
(4, 'Office In Charge'),
(5, 'Assistant Stockman');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `civil_status`
--
ALTER TABLE `civil_status`
  ADD PRIMARY KEY (`status_id`),
  ADD UNIQUE KEY `civil_status` (`civil_status`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `fk_emp` (`Employee_ID`);

--
-- Indexes for table `employment_status`
--
ALTER TABLE `employment_status`
  ADD PRIMARY KEY (`status_id`),
  ADD UNIQUE KEY `employment_status` (`employment_status`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `mechanics`
--
ALTER TABLE `mechanics`
  ADD PRIMARY KEY (`mechanics_id`),
  ADD UNIQUE KEY `mechanics` (`mechanics`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`score_id`),
  ADD UNIQUE KEY `score` (`score`);

--
-- Indexes for table `tbl_assignment`
--
ALTER TABLE `tbl_assignment`
  ADD PRIMARY KEY (`assignment_ID`);

--
-- Indexes for table `tbl_designation`
--
ALTER TABLE `tbl_designation`
  ADD PRIMARY KEY (`designation_ID`);

--
-- Indexes for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  ADD PRIMARY KEY (`driver_ID`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD UNIQUE KEY `Employee_ID` (`Employee_ID`),
  ADD KEY `designation_ID` (`designation_ID`),
  ADD KEY `assignment_ID` (`assignment_ID`),
  ADD KEY `fk_civil_status` (`CivilStatus`),
  ADD KEY `fk_employment` (`Employment_Status`);

--
-- Indexes for table `tbl_evaluatedby`
--
ALTER TABLE `tbl_evaluatedby`
  ADD PRIMARY KEY (`evaluatedID`);

--
-- Indexes for table `tbl_evaluation`
--
ALTER TABLE `tbl_evaluation`
  ADD PRIMARY KEY (`evaluation_ID`),
  ADD UNIQUE KEY `Employee_ID` (`Employee_ID`),
  ADD UNIQUE KEY `evaluated_ID` (`evaluated_by`),
  ADD KEY `fk_comments` (`comments`);

--
-- Indexes for table `tbl_items`
--
ALTER TABLE `tbl_items`
  ADD PRIMARY KEY (`itemID`);

--
-- Indexes for table `tbl_lime_spreader_helper`
--
ALTER TABLE `tbl_lime_spreader_helper`
  ADD PRIMARY KEY (`lime_spreader_helper_ID`);

--
-- Indexes for table `tbl_maintenance`
--
ALTER TABLE `tbl_maintenance`
  ADD PRIMARY KEY (`maintenance_ID`);

--
-- Indexes for table `tbl_motorpool`
--
ALTER TABLE `tbl_motorpool`
  ADD PRIMARY KEY (`motorpool_ID`);

--
-- Indexes for table `tbl_office`
--
ALTER TABLE `tbl_office`
  ADD PRIMARY KEY (`Office_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `civil_status`
--
ALTER TABLE `civil_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employment_status`
--
ALTER TABLE `employment_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `userID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mechanics`
--
ALTER TABLE `mechanics`
  MODIFY `mechanics_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_assignment`
--
ALTER TABLE `tbl_assignment`
  MODIFY `assignment_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_designation`
--
ALTER TABLE `tbl_designation`
  MODIFY `designation_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  MODIFY `driver_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `Employee_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `tbl_evaluatedby`
--
ALTER TABLE `tbl_evaluatedby`
  MODIFY `evaluatedID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_evaluation`
--
ALTER TABLE `tbl_evaluation`
  MODIFY `evaluation_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_items`
--
ALTER TABLE `tbl_items`
  MODIFY `itemID` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_lime_spreader_helper`
--
ALTER TABLE `tbl_lime_spreader_helper`
  MODIFY `lime_spreader_helper_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_maintenance`
--
ALTER TABLE `tbl_maintenance`
  MODIFY `maintenance_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_motorpool`
--
ALTER TABLE `tbl_motorpool`
  MODIFY `motorpool_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_office`
--
ALTER TABLE `tbl_office`
  MODIFY `Office_ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_emp` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD CONSTRAINT `fk_assignment` FOREIGN KEY (`assignment_ID`) REFERENCES `tbl_assignment` (`assignment_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_civil_status` FOREIGN KEY (`CivilStatus`) REFERENCES `civil_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employment` FOREIGN KEY (`Employment_Status`) REFERENCES `employment_status` (`status_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_employee_ibfk_1` FOREIGN KEY (`designation_ID`) REFERENCES `tbl_designation` (`designation_ID`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_evaluation`
--
ALTER TABLE `tbl_evaluation`
  ADD CONSTRAINT `fk_comments` FOREIGN KEY (`comments`) REFERENCES `comments` (`comment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_evaluated` FOREIGN KEY (`evaluated_by`) REFERENCES `tbl_evaluatedby` (`evaluatedID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_evaluation_ibfk_3` FOREIGN KEY (`Employee_ID`) REFERENCES `tbl_employee` (`Employee_ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
